![screenshot](https://i.imgur.com/4128luj.png)  

## Basic Reversed Steam skin
Install it via terminal:  
```bash
git clone https://github.com/thepante/steam-reversed-basic.git && mv "./steam-reversed-basic" ~/.steam/steam/skins/"Basic Reversed"
```

Then in Steam open `Settings` → `Interface`, and select the new skin `Basic Reversed`.  
  

## Changes made  
 - Window control buttons to the left
 - Typical macOS buttons
 - Removed fullscreen (Big Picture) and VR buttons (use keybinds or menu instead)
 - Hide big ass "friends" button. Now is the account profile picture, click on it and friend list will appears

---
Tested only on Linux, shouldn't have errors under Windows.
